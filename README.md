# Kotahi Pub - demo journal

This repo uses Coko’s Flax platform to build the html page for each article published on Aperture Neuro.

## How it works

### Content and layout

#### Fetch the content for each article from Kotahi

In `/src/data/config.js` you can set the graphql url.

#### Layouts

You can add pages by adding markdown, nunjucks or html files (`md`, `njk`, `html`) in `/src/content/`

In the frontmatter of those page, you can add the permalink and the `layout` you want to use like this:

```yaml
---
title: title of the page
layout: single.njk
permalink: /path-on-the-site/title-of-the-page.html
---
```

- The layout for the index of the all the articles are in `/src/articleList.njk`. It’s a simple List with an introduction set in the `index.md`
- Each article is set as a single HTML page using `/src/articlePreview.njk`
- Single page can use `single.njk`
- Team page can be set using `team.njk` to fetch data from `src/data/team.json`

### assets

The assets in this demo files are stored under a json file in `src/data/assets.json`. It allows any valid HTML. They are pulled by the njks files using `{{assets.object}}`. If you want to change the licence for example, or the logo, you can change any entry in the json and it gets pulled by the system.
Make sure that your HTML is on one line and has double quotes escaped as in any `json` string.
